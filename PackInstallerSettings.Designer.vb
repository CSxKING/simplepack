﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PackInstallerSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PackInstallerSettings))
        Me.GetModLoaderCheckbox = New MaterialSkin.Controls.MaterialCheckBox()
        Me.MaterialCheckBox1 = New MaterialSkin.Controls.MaterialCheckBox()
        Me.CustomMinecraftDirBox = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.ClearPackURLBtn = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.MaterialCheckBox2 = New MaterialSkin.Controls.MaterialCheckBox()
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.MaterialLabel1 = New MaterialSkin.Controls.MaterialLabel()
        Me.SuspendLayout()
        '
        'GetModLoaderCheckbox
        '
        Me.GetModLoaderCheckbox.AutoSize = True
        Me.GetModLoaderCheckbox.Depth = 0
        Me.GetModLoaderCheckbox.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.GetModLoaderCheckbox.Location = New System.Drawing.Point(9, 9)
        Me.GetModLoaderCheckbox.Margin = New System.Windows.Forms.Padding(0)
        Me.GetModLoaderCheckbox.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.GetModLoaderCheckbox.MouseState = MaterialSkin.MouseState.HOVER
        Me.GetModLoaderCheckbox.Name = "GetModLoaderCheckbox"
        Me.GetModLoaderCheckbox.Ripple = True
        Me.GetModLoaderCheckbox.Size = New System.Drawing.Size(178, 30)
        Me.GetModLoaderCheckbox.TabIndex = 0
        Me.GetModLoaderCheckbox.Text = "Install ModLoader?"
        Me.GetModLoaderCheckbox.UseVisualStyleBackColor = True
        '
        'MaterialCheckBox1
        '
        Me.MaterialCheckBox1.AutoSize = True
        Me.MaterialCheckBox1.Depth = 0
        Me.MaterialCheckBox1.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.MaterialCheckBox1.Location = New System.Drawing.Point(312, 9)
        Me.MaterialCheckBox1.Margin = New System.Windows.Forms.Padding(0)
        Me.MaterialCheckBox1.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.MaterialCheckBox1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialCheckBox1.Name = "MaterialCheckBox1"
        Me.MaterialCheckBox1.Ripple = True
        Me.MaterialCheckBox1.Size = New System.Drawing.Size(303, 30)
        Me.MaterialCheckBox1.TabIndex = 1
        Me.MaterialCheckBox1.Text = "Custom Minecraft Install Directory?"
        Me.MaterialCheckBox1.UseVisualStyleBackColor = True
        '
        'CustomMinecraftDirBox
        '
        Me.CustomMinecraftDirBox.Depth = 0
        Me.CustomMinecraftDirBox.Hint = ""
        Me.CustomMinecraftDirBox.Location = New System.Drawing.Point(12, 42)
        Me.CustomMinecraftDirBox.MouseState = MaterialSkin.MouseState.HOVER
        Me.CustomMinecraftDirBox.Name = "CustomMinecraftDirBox"
        Me.CustomMinecraftDirBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CustomMinecraftDirBox.SelectedText = ""
        Me.CustomMinecraftDirBox.SelectionLength = 0
        Me.CustomMinecraftDirBox.SelectionStart = 0
        Me.CustomMinecraftDirBox.Size = New System.Drawing.Size(352, 28)
        Me.CustomMinecraftDirBox.TabIndex = 2
        Me.CustomMinecraftDirBox.Text = "Enter Custom Minecraft Directory Here"
        Me.CustomMinecraftDirBox.UseSystemPasswordChar = False
        '
        'ClearPackURLBtn
        '
        Me.ClearPackURLBtn.BackColor = System.Drawing.Color.White
        Me.ClearPackURLBtn.Depth = 0
        Me.ClearPackURLBtn.Location = New System.Drawing.Point(12, 76)
        Me.ClearPackURLBtn.MouseState = MaterialSkin.MouseState.HOVER
        Me.ClearPackURLBtn.Name = "ClearPackURLBtn"
        Me.ClearPackURLBtn.Primary = True
        Me.ClearPackURLBtn.Size = New System.Drawing.Size(632, 33)
        Me.ClearPackURLBtn.TabIndex = 3
        Me.ClearPackURLBtn.Text = "Clear ModPack JSON"
        Me.ClearPackURLBtn.UseVisualStyleBackColor = False
        '
        'MaterialCheckBox2
        '
        Me.MaterialCheckBox2.AutoSize = True
        Me.MaterialCheckBox2.Depth = 0
        Me.MaterialCheckBox2.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.MaterialCheckBox2.Location = New System.Drawing.Point(367, 39)
        Me.MaterialCheckBox2.Margin = New System.Windows.Forms.Padding(0)
        Me.MaterialCheckBox2.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.MaterialCheckBox2.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialCheckBox2.Name = "MaterialCheckBox2"
        Me.MaterialCheckBox2.Ripple = True
        Me.MaterialCheckBox2.Size = New System.Drawing.Size(248, 30)
        Me.MaterialCheckBox2.TabIndex = 4
        Me.MaterialCheckBox2.Text = "Show Changelog on launch?"
        Me.MaterialCheckBox2.UseVisualStyleBackColor = True
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(12, 115)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(632, 23)
        Me.MaterialRaisedButton1.TabIndex = 5
        Me.MaterialRaisedButton1.Text = "Save"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        '
        'MaterialLabel1
        '
        Me.MaterialLabel1.AutoSize = True
        Me.MaterialLabel1.Depth = 0
        Me.MaterialLabel1.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel1.Location = New System.Drawing.Point(190, 9)
        Me.MaterialLabel1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel1.Name = "MaterialLabel1"
        Me.MaterialLabel1.Size = New System.Drawing.Size(136, 24)
        Me.MaterialLabel1.TabIndex = 6
        Me.MaterialLabel1.Text = "MaterialLabel1"
        '
        'PackInstallerSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 142)
        Me.Controls.Add(Me.MaterialLabel1)
        Me.Controls.Add(Me.MaterialRaisedButton1)
        Me.Controls.Add(Me.MaterialCheckBox2)
        Me.Controls.Add(Me.ClearPackURLBtn)
        Me.Controls.Add(Me.CustomMinecraftDirBox)
        Me.Controls.Add(Me.MaterialCheckBox1)
        Me.Controls.Add(Me.GetModLoaderCheckbox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(637, 141)
        Me.Name = "PackInstallerSettings"
        Me.Text = "ModPack Settings"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GetModLoaderCheckbox As MaterialSkin.Controls.MaterialCheckBox
    Friend WithEvents MaterialCheckBox1 As MaterialSkin.Controls.MaterialCheckBox
    Friend WithEvents CustomMinecraftDirBox As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents ClearPackURLBtn As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialCheckBox2 As MaterialSkin.Controls.MaterialCheckBox
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialLabel1 As MaterialSkin.Controls.MaterialLabel
End Class
