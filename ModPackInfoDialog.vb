﻿Public Class ModPackInfoDialog
    Private Sub ModPackInfoDialog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MaterialSingleLineTextField1.Text = Form1.PackName
        MaterialSingleLineTextField2.Text = Form1.ModLoaderName
        MaterialSingleLineTextField3.Text = Form1.ModLoaderVersion
        MaterialSingleLineTextField4.Text = My.Settings.CustomPackUris
        MaterialSingleLineTextField5.Text = Form1.PackAuth
        MaterialSingleLineTextField6.Text = Form1.AmountOfMods
    End Sub
End Class