﻿Public Class PackInstallerSettings
    Private Sub ClearPackURLBtn_Click(sender As Object, e As EventArgs) Handles ClearPackURLBtn.Click
        My.Settings.CustomPackUris = Nothing
        My.Settings.Save()
        MessageBox.Show("Application restart for change to take effect.")
        Form1.InstallBtn.Enabled = False
        'Button2.Enabled = False
        Me.GetModLoaderCheckbox.Enabled = False
        Me.ClearPackURLBtn.Enabled = False
        ' Change btn txt
        Form1.InstallBtn.Text = "Restart app to continue"
        'Button2.Text = "Restart app to continue"
        Me.ClearPackURLBtn.Text = "Restart app to continue"
    End Sub

    Private Sub PackInstallerSettings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GetModLoaderCheckbox.Text = "Install " & Form1.ModLoaderName & "?"
        If My.Settings.usecustommcdir = "False" Then
            MaterialCheckBox1.Checked = False
            CustomMinecraftDirBox.Enabled = False
            CustomMinecraftDirBox.Text = My.Settings.mcdir
            MaterialLabel1.Text = " "
        ElseIf My.Settings.usecustommcdir = "True" Then
            MaterialCheckBox1.Checked = True
            CustomMinecraftDirBox.Enabled = True
            CustomMinecraftDirBox.Text = My.Settings.mcdir
            MaterialLabel1.Text = " "
        End If
    End Sub

    Private Sub GetModLoaderCheckbox_CheckedChanged(sender As Object, e As EventArgs) Handles GetModLoaderCheckbox.CheckedChanged
        If GetModLoaderCheckbox.Checked Then
            Form1.InstallBtn.Text = "Install " & Form1.ModLoaderName & " & " & Form1.AmountOfMods & " Mods"
            Form1.GetModLoaderChecked = True
        Else
            Form1.InstallBtn.Text = "Install " & Form1.AmountOfMods & " Mods"
            Form1.GetModLoaderChecked = False
        End If
    End Sub

    Private Sub MaterialCheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles MaterialCheckBox2.CheckedChanged
        If MaterialCheckBox2.Checked Then
            My.Settings.ShowChangelogOnLaunch = "True"
            My.Settings.Save()
        Else
            My.Settings.ShowChangelogOnLaunch = "False"
            My.Settings.Save()
        End If
    End Sub

    Private Sub MaterialCheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles MaterialCheckBox1.CheckedChanged
        If MaterialCheckBox1.Checked = True Then
            My.Settings.usecustommcdir = "True"
            CustomMinecraftDirBox.Enabled = True
            My.Settings.Save()
            MaterialLabel1.Text = " "
        ElseIf MaterialCheckBox1.Checked = False Then
            My.Settings.usecustommcdir = "False"
            CustomMinecraftDirBox.Enabled = False
            My.Settings.Save()
            MaterialLabel1.Text = " "
        End If
    End Sub

    Private Sub CustomMinecraftDirBox_Click(sender As Object, e As EventArgs) Handles CustomMinecraftDirBox.Click
        My.Settings.mcdir = CustomMinecraftDirBox.Text
        My.Settings.Save()
    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        If MaterialCheckBox1.Checked = True Then
            My.Settings.usecustommcdir = "True"
            My.Settings.Save()
        ElseIf MaterialCheckBox1.Checked = False Then
            My.Settings.usecustommcdir = "False"
            My.Settings.Save()
        End If
        My.Settings.mcdir = CustomMinecraftDirBox.Text
        My.Settings.Save()
    End Sub
End Class