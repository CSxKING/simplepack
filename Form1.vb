﻿Imports System.IO
Imports System.Net
Imports System.Xml
Imports Newtonsoft.Json.Linq
Imports System.Environment
Public Class Form1
    Public Shared json '= JObject.Parse(File.ReadAllText(My.Application.Info.DirectoryPath & "\pack.json"))
    Public Shared jsonLoc As String = My.Application.Info.DirectoryPath & "\pack.json"
    Public Shared AmountOfMods
    Public Shared CurrentMod As String = 0
    Public Shared CurrentModURL As String
    Public Shared CurrentModName As String
    Public Shared ModLoaderName As String
    Public Shared ModLoaderVersion As String
    Public Shared ModLoaderLink As String
    Public Shared PackName As String
    Public Shared PackAuth As String
    Public Shared jsonURI As String
    Public Shared GetModLoaderChecked As Boolean
    Public Shared FileDownloader As New WebClient
    Public Shared MCDir As String = My.Settings.mcdir
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If My.Settings.mcdir = "null" Then
            My.Settings.mcdir = GetFolderPath(SpecialFolder.ApplicationData) & "\.minecraft"
            My.Settings.Save()
        Else

        End If
        TextBox1.Enabled = False
        If My.Settings.ShowChangelogOnLaunch = "True" Then
            Changelog.Show()
            PackInstallerSettings.MaterialCheckBox2.Checked = 1
        ElseIf My.Settings.ShowChangelogOnLaunch = "False" Then


        End If
        If My.Computer.FileSystem.FileExists(jsonLoc) Then
            json = JObject.Parse(File.ReadAllText(My.Application.Info.DirectoryPath & "\pack.json"))
            CurrentModURL = json.selecttoken("mods").selecttoken("link[" & CurrentMod & "]")
            CurrentModName = json.selecttoken("mods").selecttoken("name[" & CurrentMod & "]")
            ModLoaderName = json.selecttoken("modloader.name")
            ModLoaderVersion = json.selecttoken("modloader.version")
            ModLoaderLink = json.selecttoken("modloader.link")
            PackName = json.selecttoken("name")
            PackAuth = json.selecttoken("author")
            AmountOfMods = json.SelectToken("mod").SelectToken("names").Count()
            Me.Text = PackName & " Installer by " & PackAuth & " using " & ModLoaderName & " ModLoader version " & ModLoaderVersion
            TextBox1.Enabled = False
            PackInstallerSettings.GetModLoaderCheckbox.Text = "Install " & ModLoaderName & " ModLoader?"
            InstallBtn.Text = "Install " & AmountOfMods & " Mods"
        ElseIf String.IsNullOrEmpty(My.Settings.CustomPackUris) Then
            jsonURI = InputBox("Insert pack json url")
            My.Settings.CustomPackUris = jsonURI
            My.Settings.Save()
            GetInstallJSON()
        Else
            GetInstallJSON()
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub TextBox1_KeyPress(KeyAscii As Integer)
        KeyAscii = 0
    End Sub

    Function GetInstallJSON()
        Dim client2 As WebClient = New WebClient
        While client2.IsBusy = True
            InstallBtn.Text = "Installing mods..."
        End While
        AddHandler client2.DownloadFileCompleted, AddressOf client2_DownloadCompleted
        client2.DownloadFileAsync(New Uri(My.Settings.CustomPackUris), My.Application.Info.DirectoryPath & "\webpack.json")
    End Function


    Private Sub client2_DownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        json = JObject.Parse(File.ReadAllText(My.Application.Info.DirectoryPath & "\webpack.json"))
        CurrentModURL = json.selecttoken("mod").selecttoken("link[" & CurrentMod & "]")
        CurrentModName = json.selecttoken("mod").selecttoken("name[" & CurrentMod & "]")
        ModLoaderName = json.selecttoken("modloader.name")
        ModLoaderVersion = json.selecttoken("modloader.version")
        ModLoaderLink = json.selecttoken("modloader.link")
        PackName = json.selecttoken("name")
        PackAuth = json.selecttoken("author")
        AmountOfMods = json.SelectToken("mod").SelectToken("name").Count()
        Me.Text = PackName & " Installer by " & PackAuth & " using " & ModLoaderName & " ModLoader version " & ModLoaderVersion
        TextBox1.Enabled = False
        PackInstallerSettings.GetModLoaderCheckbox.Text = "Install " & ModLoaderName & " ModLoader?"
        PackInstallerSettings.ClearPackURLBtn.Enabled = True
        InstallBtn.Text = "Install " & AmountOfMods & " mods"
    End Sub
    Public Function InstallModLoader()
        Dim client1 As WebClient = New WebClient
        While client1.IsBusy = True
            InstallBtn.Text = "Installing mods..."
        End While
        AddHandler client1.DownloadProgressChanged, AddressOf client1_ProgressChanged
        AddHandler client1.DownloadFileCompleted, AddressOf client1_DownloadCompleted
        TextBox1.AppendText(TextBox1.Text & "Downloading " & ModLoaderName & " Version " & ModLoaderVersion)
        client1.DownloadFileAsync(New Uri(ModLoaderLink), My.Application.Info.DirectoryPath & "\modloader.jar")

    End Function
    Function DownloadAllMods()
        If PackInstallerSettings.GetModLoaderCheckbox.Checked = True Then
            InstallModLoader()
        End If
        Dim client As WebClient = New WebClient
        While client.IsBusy = True
            InstallBtn.Text = "Installing mods..."
        End While
        AddHandler client.DownloadProgressChanged, AddressOf client_ProgressChanged
        AddHandler client.DownloadFileCompleted, AddressOf client_DownloadCompleted
        If CurrentMod < AmountOfMods Then
            client.DownloadFileAsync(New Uri(CurrentModURL), My.Application.Info.DirectoryPath & "\mods\" & CurrentModName & ".jar")
            InstallBtn.Text = "Installing..."
            InstallBtn.Enabled = False
            'Button2.Enabled = False
            PackInstallerSettings.ClearPackURLBtn.Enabled = False
            CurrentMod += 1
            CurrentModURL = json.selecttoken("mod").selecttoken("link[" & CurrentMod & "]")
            CurrentModName = json.selecttoken("mod").selecttoken("name[" & CurrentMod & "]")
            TextBox1.AppendText(Environment.NewLine & "Downloading mod " & json.selecttoken("mod").selecttoken("name[" & CurrentMod & "]"))
            DownloadAllMods()
        Else
            InstallBtn.Text = "Installing"
        End If
    End Function

    Function InstallMods()
        Threading.Thread.Sleep(500)
        Dim SourcePath As String = My.Application.Info.DirectoryPath & "\mods\"
        Dim DestPath As String = My.Settings.mcdir
        Dim newDirectory As String = System.IO.Path.Combine(DestPath, Path.GetFileName(Path.GetDirectoryName(SourcePath)))
        If Not (Directory.Exists(newDirectory)) Then
            Directory.CreateDirectory(newDirectory)
        End If
        Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(SourcePath, newDirectory, True)

        InstallBtn.Text = "Done!"
    End Function

    Private Sub client_ProgressChanged(ByVal sender As Object, ByVal e As DownloadProgressChangedEventArgs)
        Dim bytesIn As Double = Double.Parse(e.BytesReceived.ToString())
        Dim totalBytes As Double = Double.Parse(e.TotalBytesToReceive.ToString())
        Dim percentage As Double = bytesIn / totalBytes * 100
        InstallBtn.Text = "Installing mods..."
        ProgressBar.Value = Int32.Parse(Math.Truncate(percentage).ToString())
    End Sub

    Private Sub client_DownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        If CurrentMod < AmountOfMods Then
            DownloadAllMods()
        ElseIf CurrentMod >= AmountOfMods Then
            InstallBtn.Text = "Installing..."
            InstallMods()
        End If
    End Sub


    Private Sub client1_ProgressChanged(ByVal sender As Object, ByVal e As DownloadProgressChangedEventArgs)
        Dim bytesIn As Double = Double.Parse(e.BytesReceived.ToString())
        Dim totalBytes As Double = Double.Parse(e.TotalBytesToReceive.ToString())
        Dim percentage As Double = bytesIn / totalBytes * 100
        InstallBtn.Text = "Installing mods..."
        ProgressBar.Value = Int32.Parse(Math.Truncate(percentage).ToString())
    End Sub

    Private Sub client1_DownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        Process.Start(My.Application.Info.DirectoryPath & "\modloader.jar")
        TextBox1.AppendText(Environment.NewLine & "Parsed JSON. Output: " & Environment.NewLine & json.ToString)
        TextBox1.AppendText(Environment.NewLine & "Downloading mod " & json.selecttoken("mod").selecttoken("name[" & CurrentMod & "]"))
        DownloadAllMods()
    End Sub


    'Private Sub Button1_Click(sender As Object, e As EventArgs)
    'If Not Directory.Exists(My.Application.Info.DirectoryPath & "\mods") Then
    'Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\mods")
    'End If
    'TextBox1.AppendText(Environment.NewLine & "Parsed JSON. Output: " & Environment.NewLine & json.ToString)
    'TextBox1.AppendText(Environment.NewLine & "Downloading mod " & json.selecttoken("mod").selecttoken("name[" & CurrentMod & "]"))
    'DownloadAllMods()
    'End Sub
    Private Shared Sub CopyDirectory(ByVal sourcePath As String, ByVal destPath As String)
        If Not Directory.Exists(destPath) Then
            Directory.CreateDirectory(destPath)
        End If

        For Each file__1 As String In Directory.GetFiles(Path.GetDirectoryName(sourcePath))
            Dim dest As String = Path.Combine(destPath, Path.GetFileName(file__1))
            File.Copy(file__1, dest)
        Next

        For Each folder As String In Directory.GetDirectories(Path.GetDirectoryName(sourcePath))
            Dim dest As String = Path.Combine(destPath, Path.GetFileName(folder))
            CopyDirectory(folder, dest)
        Next
    End Sub

    Private Sub Form1_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\webpack.json") Then
            My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\webpack.json")
        End If
    End Sub

    Private Sub SettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SettingsToolStripMenuItem.Click
        PackInstallerSettings.Show()
    End Sub

    Private Sub InstallBtn_Click(sender As Object, e As EventArgs) Handles InstallBtn.Click
        If GetModLoaderChecked = True Then
            InstallModLoader()
        End If
        If GetModLoaderChecked = False Then
            DownloadAllMods()
        End If

        If Not Directory.Exists(My.Application.Info.DirectoryPath & "\mods") Then
            Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\mods")
        End If
    End Sub

    Private Sub ModpackInfoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModpackInfoToolStripMenuItem.Click
        ModPackInfoDialog.show()
    End Sub

    Private Sub CreditsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CreditsToolStripMenuItem.Click
        CreditsDialog.show()
    End Sub

    Private Sub MenuStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles MenuStrip1.ItemClicked

    End Sub
End Class
